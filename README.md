# RLH Defaults

## A Features module to jump-start a Drupal site

This module does a lot of stuff. Some of it (like restoring my favored CKEditor profile config) is good, and some of it (the default Paragraphs bundles that are very specific to the site I created them on) is probably not. Plus, I haven't quite figured out a good way to use Features to import all this stuff, then entirely get rid of the module and its dependencies while keeping all its effects.

In short, there may be some interesting stuff in here, but think long and hard before actually installing it on a production site.