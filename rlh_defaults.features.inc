<?php
/**
 * @file
 * rlh_defaults.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rlh_defaults_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function rlh_defaults_node_info() {
  $items = array(
    'downloadable_file' => array(
      'name' => t('Downloadable file'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function rlh_defaults_paragraphs_info() {
  $items = array(
    'block' => array(
      'name' => 'Drupal block',
      'bundle' => 'block',
      'locked' => '1',
    ),
    'body_text' => array(
      'name' => 'Body text',
      'bundle' => 'body_text',
      'locked' => '1',
    ),
    'centered_text' => array(
      'name' => 'Centered text',
      'bundle' => 'centered_text',
      'locked' => '1',
    ),
    'page_heading' => array(
      'name' => 'Page heading',
      'bundle' => 'page_heading',
      'locked' => '1',
    ),
    'sharable_graphic' => array(
      'name' => 'Sharable graphic',
      'bundle' => 'sharable_graphic',
      'locked' => '1',
    ),
    'text_block_rows' => array(
      'name' => 'Text blocks in rows (with optional icons)',
      'bundle' => 'text_block_rows',
      'locked' => '1',
    ),
    'text_graphic' => array(
      'name' => 'Text + Graphic',
      'bundle' => 'text_graphic',
      'locked' => '1',
    ),
  );
  return $items;
}
