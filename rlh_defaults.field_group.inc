<?php
/**
 * @file
 * rlh_defaults.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function rlh_defaults_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_graphic|paragraphs_item|text_graphic|form';
  $field_group->group_name = 'group_graphic';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_graphic';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Graphic',
    'weight' => '3',
    'children' => array(
      0 => 'field_image_retina',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-graphic field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_graphic|paragraphs_item|text_graphic|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|block|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|body_text|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'body_text';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|body_text|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|centered_text|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'centered_text';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '0',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|centered_text|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|page_heading|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'page_heading';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|page_heading|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|sharable_graphic|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'sharable_graphic';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|sharable_graphic|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|text_block_rows|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_block_rows';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_background_color',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|text_block_rows|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|paragraphs_item|text_graphic|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_graphic';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '1',
    'children' => array(
      0 => 'field_image_side',
      1 => 'field_background_color',
      2 => 'field_image_sizing',
      3 => 'field_content_sizing',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings|paragraphs_item|text_graphic|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_blocks|paragraphs_item|text_block_rows|form';
  $field_group->group_name = 'group_text_blocks';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_block_rows';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text blocks',
    'weight' => '4',
    'children' => array(
      0 => 'field_text_block',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Text blocks',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-text-blocks field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_text_blocks|paragraphs_item|text_block_rows|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_inner|paragraphs_item|text_graphic|default';
  $field_group->group_name = 'group_text_inner';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_graphic';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_text';
  $field_group->data = array(
    'label' => 'Text inner',
    'weight' => '9',
    'children' => array(
      0 => 'field_heading',
      1 => 'field_body',
      2 => 'field_link',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text inner',
      'instance_settings' => array(
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_text_inner|paragraphs_item|text_graphic|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text|paragraphs_item|text_graphic|default';
  $field_group->group_name = 'group_text';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_graphic';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text',
    'weight' => '0',
    'children' => array(
      0 => 'group_text_inner',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Text',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-text',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_text|paragraphs_item|text_graphic|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text|paragraphs_item|text_graphic|form';
  $field_group->group_name = 'group_text';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'text_graphic';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text',
    'weight' => '2',
    'children' => array(
      0 => 'field_heading',
      1 => 'field_body',
      2 => 'field_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-text field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_text|paragraphs_item|text_graphic|form'] = $field_group;

  return $export;
}
