<?php
/**
 * @file
 * rlh_defaults.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rlh_defaults_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_to_downloadable_file'] = entity_import('rules_config', '{ "rules_redirect_to_downloadable_file" : {
    "LABEL" : "Redirect to downloadable file",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "REQUIRES" : [ "php", "rules" ],
    "ON" : { "node_view--downloadable_file" : { "bundle" : "downloadable_file" } },
    "IF" : [
      { "NOT php_eval" : { "code" : "return node_access(\u0027update\u0027, $node, $user);" } }
    ],
    "DO" : [
      { "redirect" : { "url" : "\u003C?php\r\n$file =  field_get_items(\u0027node\u0027, $node, \u0027field_file\u0027);\r\nprint file_create_url($file[0][\u0027uri\u0027]);\r\n?\u003E" } }
    ]
  }
}');
  return $items;
}
